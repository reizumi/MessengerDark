# Messenger Dark
A dark theme for Facebook Messenger.

[![Version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https://codeberg.org/api/v1/repos/reizumi/MessengerDark/tags&query=$[0].name)](https://codeberg.org/reizumi/MessengerDark/tags) [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://codeberg.org/reizumi/MessengerDark/raw/branch/master/messenger-dark.user.css)

![Screenshot](https://codeberg.org/reizumi/MessengerDark/raw/branch/master/images/screenshot.png)

## Features
- Comes with built-in color themes accessible through the Stylus menu
- Customizable color options (if 'Custom' color theme is selected)

## Installation
Install [Stylus](https://add0n.com/stylus.html) and this theme by clicking the button below.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://codeberg.org/reizumi/MessengerDark/raw/branch/master/messenger-dark.user.css)
